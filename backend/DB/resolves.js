var bd = require('./sqliteConnector.js');

function getAllCharacters() {
  return new Promise((resolve, reject) => {
    try {
      console.log("getAllCharacters");
      
      bd.query(`SELECT * FROM characters`)
        .then(([results, metadata]) => {
          console.log("resolve");
          resolve(results)
        })
        .catch((err) => {
          console.log("ERR: ", err);
          reject(err)
        })
    } catch (error) {
      console.log("CATCH ERROR: ", error);
      reject(err)
    }
  })
}

function getIdCharacters(id) {
  return new Promise((resolve, reject) => {
    try {
      console.log("get Id Characters");
      
      bd.query(`SELECT * FROM characters WHERE id = '${id}'`)
        .then(([results, metadata]) => {
          resolve(results)
        })
        .catch((err) => {
          console.log("ERR: ", err);
          reject(err)
        })
    } catch (error) {
      console.log("CATCH ERROR: ", error);
      reject(err)
    }
  })
}

function insertCharacter(obj) {
  return new Promise((resolve, reject) => {
    try {
      console.log("insert Character");
      
      bd.query(`INSERT INTO characters (name, gender, slug, rank, house, books, titles) VALUES ('${obj.name}', '${obj.gender}', '${obj.slug}', '${obj.rank}', '${obj.house}', '${obj.book}', '${obj.titles}')`)
        .then(([results, metadata]) => {
          resolve(results)
        })
        .catch((err) => {
          console.log("ERR: ", err);
          reject(err)
        })
    } catch (error) {
      console.log("CATCH ERROR: ", error);
      reject(err)
    }
  })
}

function deleteCharacter(obj) {
  return new Promise((resolve, reject) => {
    try {
      console.log("Truncate Character");
      
      bd.query(`DELETE FROM characters;`)
        .then(([results, metadata]) => {
          bd.query(`UPDATE sqlite_sequence SET seq=0 WHERE name='characters';`)
            .then(([results, metadata]) => {
              resolve(results)
            })
            .catch((err) => {
              console.log("ERR: ", err);
              reject(err)
            })
        })
        .catch((err) => {
          console.log("ERR: ", err);
          reject(err)
        })
      
    } catch (error) {
      console.log("CATCH ERROR: ", error);
      reject(err)
    }
  })
}

module.exports = {
  getAllCharacters,
  getIdCharacters,
  insertCharacter,
  deleteCharacter
}