const Sequelize = require('sequelize');

let db = null;

if (!db) {
  db = new Sequelize({
    dialect: 'sqlite',
    storage: "./DB/dbprueba.db"
  });
}

module.exports = db;
