const express = require('express');
const router = express.Router();
var request = require("request");

var {getAllCharacters, getIdCharacters, insertCharacter, deleteCharacter} = require("../DB/resolves.js")


router.get(
  "/characters/:id",
  async (req, res) => {
    try {      
      console.log("/characters/:id", req.params.id);
      let rt = await getIdCharacters(req.params.id);
      console.log(`routes.js get /charaters/:id ${JSON.stringify(rt)}`);
      res.status(200).json(rt);
    } catch (error) {
      console.log("CATCH routes.js /charaters/:id :" + error);
      res.status(500).json({status: "ERROR"});
    };
  }
);

router.get(
  "/characters",
  async (req, res) => {
    try {
      console.log("/characters");
      let rt = await getAllCharacters();
      // console.log(`routes.js get /charaters ${JSON.stringify(rt)}`);
      res.status(200).json(rt);
    } catch (error) {
      console.log("CATCH routes.js /charaters :" + error);
      res.status(500).json({status: "ERROR"});
    };
  }
);

var maxReg = 100;
router.post("/updateDB", (req, res) => {
  try {
    console.log("/updateDB");

    let options = {
      method: 'GET',
      url: 'https://api.got.show/api/book/characters',
    };

    request(options, async (error, response, body) => {
      if (error) {
        console.log(`Catch routes.js updateDB Error: ${error}`);
        res.status(500).json({status: "ERROR"});
      } else {
        if (body.length > 0) {
          body = JSON.parse(body)
           // maxReg = body.length         
          await deleteCharacter();

          for (let i = 0; i < maxReg; i++) {
            console.log(`routes.js /updateDB ${i} de ${maxReg}:`);
            
            let obj = {
              name: body[i].name,
              gender: body[i].gender,
              slug: body[i].slug,
              rank: body[i].pagerank != undefined ? body[i].pagerank.rank : "null",
              house: body[i].house,
              book: body[i].books.join(" - "),
              titles: body[i].titles.join(" - ")
            };

            try {
              await insertCharacter(obj);  
            } catch (error) {
              console.log("Error al insertar");
            }
            
            if (i == (maxReg - 1)) {
              console.log("Insert Completed");
              
              res.status(200).json({status: "OK", cant: maxReg});
            }
          }
        } else {
          res.status(200).json({status: "OK"});

        }
      }
    });
  } catch (error) {
    console.log("CATCH routes.js /updateDB:" + error);
    res.status(500).json({status: "ERROR"});
  }
})

module.exports = router;