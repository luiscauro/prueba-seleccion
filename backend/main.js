// Declaracion de
const express = require("express"),
  bodyParser = require("express");
var routes = require("./routes/routes.js");

var axios = require("request")

const app = express();
// parse application/json
app.use(bodyParser.json({limit: '50mb'}));

app.use(routes);

// app.use(express.static(path.join(__dirname + "/dist_vue")));
app.use(express.static("../frontend/dist"));


app.listen(
  2703, 
  () => {}
);


